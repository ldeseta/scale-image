package com.dosideas.scale;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import javax.imageio.stream.FileImageOutputStream;
import org.imgscalr.Scalr;

public class App {

    private static final int WIDTH = 1080;
    private static final float COMPRESSION_QUALITY = 0.6f;

    public static void main(String[] args) throws IOException {
//        InputStream imageAsResource = App.class.getResourceAsStream("/sample.jpg");
        InputStream imageAsResource = new FileInputStream("C:\\Users\\Leito\\Downloads\\sample.jpg");

        BufferedImage image = ImageIO.read(imageAsResource);

        System.out.println("Width: " + image.getWidth());
        System.out.println("Height: " + image.getHeight());

        if (image.getWidth() > WIDTH) {
            image = resizeImageWidth(image, WIDTH);
        }

        System.out.println("Width: " + image.getWidth());
        System.out.println("Height: " + image.getHeight());

        saveJpg(image, COMPRESSION_QUALITY);
    }

    private static BufferedImage resizeImageWidth(BufferedImage originalImage, int width) {
        return Scalr.resize(originalImage, Scalr.Method.QUALITY, width);
    }

    private static BufferedImage resizeImageWidthAwt(BufferedImage originalImage, int width) {
        int originalWidth = originalImage.getWidth();
        int originalHeight = originalImage.getHeight();
        int height = (width * originalHeight) / originalWidth;

        Image scaledImage = originalImage.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        BufferedImage bufferedImage = new BufferedImage(scaledImage.getWidth(null), scaledImage.getHeight(null), originalImage.getType());
        bufferedImage.getGraphics().drawImage(scaledImage, 0, 0, null);
        return bufferedImage;
    }

    private static void saveJpg(BufferedImage img, float compressionQuality) throws IOException {
        JPEGImageWriteParam jpegParams = new JPEGImageWriteParam(null);
        jpegParams.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
        jpegParams.setCompressionQuality(compressionQuality);

        ImageWriter writer = ImageIO.getImageWritersByFormatName("jpg").next();
        writer.setOutput(new FileImageOutputStream(new File("C:\\Users\\Leito\\Downloads\\sample-resize.jpg")));
        writer.write(null, new IIOImage(img, null, null), jpegParams);
    }

}
